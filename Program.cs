﻿using System;

namespace MotivatorHW
{
    class Program
    {
        static void Main(string[] args)
        {
            var motivator = new MotivatorFacade();
            
            // Sample 1
            var response = motivator.CreateMotivator(".\\input1.jpg", "Text for input 1");
            Console.WriteLine(response);

            // Sample 2
            response = motivator.CreateMotivator(".\\input2.jpg", "Some different text for input 2");
            Console.WriteLine(response);
        }
    }
}
