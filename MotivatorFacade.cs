﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace MotivatorHW
{
    internal class MotivatorFacade
    {
        private const int Width = 800;
        private const int Height = 700;
        private const int Margin = 20;
        private const int BorderThickness = 5;
        private const int MinimalTextHeight = 75; // Must be greater than Margin
        private const string MotivatorSuffix = "_motivator.jpg";

        private readonly Color _backgroundColor = Color.Black;
        private readonly Color _borderColor = Color.Yellow;
        private readonly Color _fontColor = Color.White;

        public string CreateMotivator(string image, string message)
        {
            var inputImage = new Bitmap(Image.FromFile(image));
            inputImage = ResizeImage(inputImage);

            var motivatorImage = new Bitmap(Width, Height);
            using (var graphics = Graphics.FromImage(motivatorImage))
            {
                // Creating background
                graphics.FillRectangle(new SolidBrush(_backgroundColor), 0, 0, Width, Height);
                // Creating border
                int xBorderStart = (int)Math.Round(Width / 2f - inputImage.Width / 2f, 0);
                graphics.FillRectangle(new SolidBrush(_borderColor),
                    xBorderStart - 1, Margin - 1,
                    inputImage.Width + 2 * BorderThickness, inputImage.Height + 2 * BorderThickness);
                // Inserting image
                graphics.DrawImage(inputImage,
                    xBorderStart + BorderThickness - 1,
                    Margin + BorderThickness - 1);
                // Inserting text
                int maxTextWidth = Width - 2 * Margin;
                int maxTextHeight = Height - 3 * Margin - 2 * BorderThickness - inputImage.Height;
                var fontSize = graphics.MeasureString(message, new Font(FontFamily.GenericSansSerif, 10f));
                float textScaleFactor = Math.Min(1f * maxTextWidth / fontSize.Width, 1f * maxTextHeight / fontSize.Height);
                fontSize = graphics.MeasureString(message, new Font(FontFamily.GenericSansSerif, 10f * textScaleFactor));
                graphics.DrawString(message, new Font(FontFamily.GenericSansSerif, 10f * textScaleFactor), new SolidBrush(_fontColor),
                    (int) Math.Round(Width / 2f - fontSize.Width / 2f, 0) + Margin - 1,
                    (int) Math.Round(maxTextHeight / 2f - fontSize.Height / 2f, 0) + 2 * Margin + 2 * BorderThickness + inputImage.Height - 1);
            }

            string fileName = Path.GetFileNameWithoutExtension(image);
            motivatorImage.Save($"{fileName}{MotivatorSuffix}", ImageFormat.Jpeg);
            return Path.Combine(Path.GetDirectoryName(image), fileName + MotivatorSuffix);
        }

        private Bitmap ResizeImage(Bitmap inputImage)
        {
            const int sumOfMargins = 2 * Margin + 2 * BorderThickness;
            const int maxWidth = Width - sumOfMargins;
            const int maxHeight = Height - sumOfMargins - MinimalTextHeight;

            float scaleFactor = Math.Min(1f * maxWidth / inputImage.Width, 1f * maxHeight / inputImage.Height);

            int newWidth = (int)Math.Round(inputImage.Width * scaleFactor, 0);
            int newHeight = (int)Math.Round(inputImage.Height * scaleFactor, 0);

            var resized = new Bitmap(newWidth, newHeight);
            using var graphics = Graphics.FromImage(resized);
            graphics.DrawImage(inputImage, 0, 0, newWidth, newHeight);

            return resized;
        }
    }
}
